export enum Configuration {
  API_PORT = 'API_PORT',
  DBHOST = 'DBHOST',
  DB_USERNAME = 'DB_USERNAME',
  DB_PASSWORD = 'DB_PASSWORD',
  DB_NAME = 'DB_NAME',
  DB_PORT = 'DB_PORT',
}
