import { Module } from '@nestjs/common';
import { HnewController } from './hnew.controller';
import { HnewService } from './hnew.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HnewSchema } from './schemas/hnew.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Hnew', schema: HnewSchema }])],
  controllers: [HnewController],
  providers: [HnewService],
})
export class HnewModule {}
