export class GetHnewsDTO {
  objectID: string;
  title: string;
  url: string;
  author: string;
  story_title: string;
  story_url: string;
  created_at_i: number;
}
