import { Controller, Delete, Get, Param } from '@nestjs/common';
import { HnewService } from './hnew.service';
import { Hnew } from './interfaces/hnew.interface';
@Controller('hnews')
export class HnewController {
  constructor(private readonly hnewService: HnewService) {}

  @Get()
  async getNews(): Promise<Hnew[]> {
    const hnews = await this.hnewService.getNews();
    return hnews;
  }

  @Delete('/delete/:hnewId')
  async deleteNew(@Param('hnewId') hnewId): Promise<boolean> {
    const deleted = await this.hnewService.deleteNew(hnewId);
    return deleted;
  }
}
