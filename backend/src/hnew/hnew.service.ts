import { Injectable, HttpService, OnModuleInit } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Hnew } from './interfaces/hnew.interface';
@Injectable()
export class HnewService implements OnModuleInit {
  constructor(@InjectModel('Hnew') private readonly hnewModel: Model<Hnew>) {}
  async onModuleInit(): Promise<any> {
    if (process.env.FORCE_DATA_CAPTURE) {
      console.log('Populating data');
      await this.insertNews();
    }
  }

  async getNews(): Promise<Hnew[]> {
    const news = await this.hnewModel
      .find({ deleted: false })
      .sort({ created_at_i: -1 })
      .exec();
    return news;
  }

  async deleteNew(_id: string): Promise<boolean> {
    const deleted: any = await this.hnewModel
      .update(
        { _id },
        {
          deleted: true,
        },
      )
      .exec();
    return deleted.ok;
  }

  @Cron(CronExpression.EVERY_HOUR)
  async insertNews() {
    const http = new HttpService();
    const response = await http
      .get('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    const hnews: Array<any> = response.data.hits;
    for (const hnew of hnews) {
      const objectID: string = hnew.objectID;
      const found = await this.hnewModel.find({ objectID }).exec();
      if (found.length != 0) {
      } else {
        const deleted = false;
        const hnewtoInsert: Hnew = { ...hnew, deleted };
        const newHnew = new this.hnewModel(hnewtoInsert);
        await newHnew.save();
      }
    }
  }
}
