import * as mongoose from 'mongoose';
export const HnewSchema = new mongoose.Schema({
  created_at: String,
  title: String,
  url: String,
  author: String,
  points: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: Array,
  objectID: String,
  _highlightResult: Object,
  deleted: Boolean,
});
