import { HnewService } from './hnew.service';
import { HnewModule } from './hnew.module';
import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

describe('Hnews', () => {
  let app: INestApplication;
  let hnewService = { getNews: () => [{"_tags":["comment","author_dcolkitt","story_25423113"],"_id":"5fd8137773b4ade02858a9ba","created_at":"2020-12-15T01:24:54.000Z","title":null,"url":null,"author":"dcolkitt","points":null,"story_title":"Fractal Architectures: A Software Craftsman's take to Infrastructure as Code","story_url":"https://yanchware.com/content/fractal-arch-iac","parent_id":25425716,"created_at_i":1607995494,"objectID":"25425885","_highlightResult":{"author":{"value":"dcolkitt","matchLevel":"none","matchedWords":[]},"comment_text":{"value":"&gt; I would state that simply making the arbitrary decision to use containers is a massive mistake. All that overhead and complexity for what?<p>What complexity? I've been hacking a small Node app in my dev environment for the past few weeks. Decided to stand it up in prod. It took me literally 10 minutes to setup the Dockerfile, build the image and deploy it to the server.<p>Later this week, I'll probably throw it onto a GKE cluster. It will take maybe 15 minutes to write and test the Kubernetes YAML. (It will also save 50%+ on the hosting costs, since self-healing means I'll be able to put it on GCE preemptible <em>nodes</em>.)","matchLevel":"full","fullyHighlighted":false,"matchedWords":["nodejs"]},"story_title":{"value":"Fractal Architectures: A Software Craftsman's take to Infrastructure as Code","matchLevel":"none","matchedWords":[]},"story_url":{"value":"https://yanchware.com/content/fractal-arch-iac","matchLevel":"none","matchedWords":[]}},"deleted":false,"__v":0},{"_tags":["comment","author_bob1029","story_25423113"],"_id":"5fd8137773b4ade02858a9bb","created_at":"2020-12-15T01:03:08.000Z","title":null,"url":null,"author":"bob1029","points":null,"story_title":"Fractal Architectures: A Software Craftsman's take to Infrastructure as Code","story_url":"https://yanchware.com/content/fractal-arch-iac","parent_id":25425218,"created_at_i":1607994188,"objectID":"25425716","_highlightResult":{"author":{"value":"bob1029","matchLevel":"none","matchedWords":[]},"comment_text":{"value":"I sometimes wonder how many of us are running infrastructure across multiple <em>nodes</em> in support of an application that can practically be ran on a single instance.<p>For us, having production go down for 2~5 minutes while we spin up a prior VM snapshot is totally acceptable for our customers. I have a hard time believing this RTO does not exceed what would be meaningfully required for most businesses (i.e. actual $$$ impact vs someone's paranoid fantasies).<p>I totally concur on your 10x point. If you find your engineering staff arguing over containerization technologies or multi-cloud event-driven virtual actor architectures, you are probably wasting a lot of time. I would state that simply making the arbitrary decision to use containers is a massive mistake. All that overhead and complexity for what? You better have a damn good reason. Maintaining a typical Dev/QA/Staging/Prod stack is not sufficient justification. Unless you seriously fucked something up, your software should be able to be cloned+built+deployed+started in a few lines of powershell, et. al. What is stopping you from running this script on 4 servers or writing a little tool to do it from a web interface every time you click a button? Oh right, can't write your own CI/CD tools when there are so many on the market. So it goes. Down the rabbit hole and to the right.<p>For those who can handle a 2-5 minute RTO, and have business application performance requirements that can be addressed by a single 32+ core server, there is no reason you should be screwing around with anything beyond your basic language+framework tooling, SQLite, source control, and project management tools. This is an engineering paradise if your constraints allow for it. I would never squander this opportunity with shiny bullshit.","matchLevel":"full","fullyHighlighted":false,"matchedWords":["nodejs"]},"story_title":{"value":"Fractal Architectures: A Software Craftsman's take to Infrastructure as Code","matchLevel":"none","matchedWords":[]},"story_url":{"value":"https://yanchware.com/content/fractal-arch-iac","matchLevel":"none","matchedWords":[]}},"deleted":false,"__v":0}] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [HnewModule],
    })
    .overrideProvider(HnewService)
    .useValue(hnewService)
    .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET hnews`, () => {
    return request(app.getHttpServer())
      .get('/api/hnews')
      .expect(200)
      .expect({
        data: hnewService.getNews(),
      });
  });

  afterAll(async () => {
    await app.close();
  });
});