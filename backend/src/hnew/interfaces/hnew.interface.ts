import { Document } from 'mongoose';
export interface Hnew extends Document {
  created_at: string;
  title: string;
  url: string;
  author: string;
  points: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
  _tags: Array<string>;
  objectID: string;
  _highlightResult: Record<string, unknown>;
  deleted: boolean;
}
