import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/config.keys';
import { HnewModule } from './hnew/hnew.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
const {
  DB_HOST = 'localhost',
  DB_USERNAME = 'root',
  DB_PASSWORD = 'example',
  DB_NAME = 'admin',
  DB_PORT = '27017',
} = process.env;
@Module({
  imports: [
    MongooseModule.forRoot(
      `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      },
    ),
    ScheduleModule.forRoot(),
    ConfigModule,
    HnewModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number | string;

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.API_PORT);
  }
}
