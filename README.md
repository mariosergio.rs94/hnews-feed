# Hnews Feed Services

### Prerequisites
+ [Docker](https://docs.docker.com/install/)
+ [Docker compose](https://docs.docker.com/compose/install/)

### Gettting Started

Create file '.dev.env' on base directory
include follow enviroments on this file:

```sh
API_PORT=5000
API_HOST=localhost
DB_HOST=db
DB_USERNAME=root
DB_PASSWORD=example
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=example
DB_NAME=admin
DB_PORT=27017
NODE_ENV=production
```

### Force data capture
Add this Variable to de .dev.env file

```sh
FORCE_DATA_CAPTURE=true
```

### Run project

Just run follow command
```sh
$ docker-compose up -d
```

## Read the best hacker news
Go to http://localhost:8080
