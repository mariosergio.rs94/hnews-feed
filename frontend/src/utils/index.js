import moment from 'moment'
export const getHumanDate = (created_at) => {
  let displayTime
  const today = moment().startOf('day').format('MMM DD')
  const yesterday = moment().add(-1, 'days').startOf('day').format('MMM DD')
  const time = moment.unix(created_at).startOf('day').format('MMM DD')
  if (today == time) {
    displayTime = moment.unix(created_at).format('hh:mm a')
  } else if (yesterday == time) {
    displayTime = 'Yesterday'
  } else {
    displayTime = moment.unix(created_at).format('MMM DD')
  }
  return displayTime
}
