import React, { useState, useEffect } from 'react'
import { ListOfHnews } from '../components/ListOfHnews'
import { API_URI } from './../../config'
import axios from 'axios'
const ListOfHnewsContainer = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    axios(
      API_URI
    ).then((result) => {
      setData(result.data)
    })
  }, [])

  return (
    <ListOfHnews elements={data} />
  )
}

export default ListOfHnewsContainer
