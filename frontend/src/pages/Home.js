import React, { Fragment } from 'react'
import { Layout } from '../components/Layout'
import ListOfHnewsContainer from '../containers/GetNews'
export const Home = () => {
  return (<>
    <Layout title='HN Feed' subtitle='We <3 hacker news!' />
    <ListOfHnewsContainer />
  </>
  )
}
