import { FaTrashAlt } from 'react-icons/fa'
import React from 'react'

export const DeleteButton = ({ onClick }) => {
  return (
    <button onClick={onClick}>
      <FaTrashAlt size='16px' />
    </button>
  )
}
