import styled from 'styled-components'

export const Header = styled.div`
    padding :50px 30px 50px 30px;
    background-color:#333333;
`
export const Title = styled.h1`
    font-size :60px;
    font-weight: 10;
    font-family:'Fredoka One';
    color: white;
    padding-bottom: 8px;
`
export const Subtitle = styled.h1`
    font-size :18px;
    font-family:'Varela Round';
    font-weight: bold;
    color: white;
    padding-bottom: 4px
`
