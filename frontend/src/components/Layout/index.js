import React, { Fragment } from 'react'
import { Helmet } from 'react-helmet'
import {  Title, Subtitle, Header } from './styles'
export const Layout = ({ children, title, subtitle }) => {
  return (
    <>
      <Helmet>
        {title && <title>{title} </title>}
        {subtitle && <meta name='description' content={subtitle} />}
        <meta name='HN Feed' content='HackerNews Feed' />
      </Helmet>
      <Header>
        {title && <Title>{title}</Title>}
        {subtitle && <Subtitle>{subtitle}</Subtitle>}
      </Header>

    </>

  )
}
