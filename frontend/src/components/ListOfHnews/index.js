import React from 'react'
import { Grid } from './styles'
import { Hnew } from './../Hnew'
import { getHumanDate } from './../../utils'
export const ListOfHnews = ({ elements = [] }) => {
  return <Grid>
    {
      elements.map(element => {
        const { author, _id: id } = element
        const title = element.story_title ? element.story_title : element.title
        const url = element.story_url ? element.story_url : element.url
        const time = getHumanDate(element.created_at_i)
        if (url) {
          const hnew = { title, url, time, author, id }
          return <Hnew key={element._id} {...hnew} />
        }
      }
      )
    }
  </Grid>
}
