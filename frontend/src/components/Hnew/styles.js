import styled from 'styled-components'

export const Link = styled.a`
    text-decoration: none;  
    display:grid;  
    grid-template-columns: 90% 10%;
`
export const Item = styled.div`
    font-family:'Varela Round';
    padding:20px 0px 20px 0px;
    margin:0px;
    background-color: #fff;
    border-bottom: 1px solid #ccc; 
    display:grid;  
    grid-template-columns: 95% 5%;
    &:hover {
        background-color: #fafafa;
    }
`
export const Title = styled.span`
    padding:0px 10px 0px 10px;
    text-decoration: none;   
    color:#333 !important;
    font-size:13pt;
    border: 1px #ccc
`
export const Author = styled.span`
    padding:0px 5px 0px 5px;
    color:#999;  
`
export const Time = styled.span`
    padding:0px 5px 0px 5px;
    color:#333;
    font-size:13pt; 
`
