import React, { useState } from 'react'
import { Item, Link, Title, Author, Time } from './styles'
import { DeleteButton } from './../Shared/DeleteButton'
import axios from 'axios'
import { API_URI } from './../../../config'
export const Hnew = ({ title, url, author, time, id }) => {
  const [show, setShow] = useState(true)
  const deleteHNew = (id) => {
    axios.delete(`${API_URI}/delete/${id}`).then(() => {
      setShow(false)
    })   
  }   
  return (show &&  <Item>
                    <Link href={url}>
                      <div>
                        <Title>
                          { title}
                        </Title>
                        <Author>
                          {`- ${author} -`}
                        </Author>
                      </div>
                      <Time>
                        {time}
                      </Time>
                    </Link>
                    <DeleteButton onClick={() => deleteHNew(id)} />
                  </Item>)
}
