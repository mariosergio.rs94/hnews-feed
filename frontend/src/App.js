import React, { useContext } from 'react'
import { GlobalStyle } from './styles/GlobalStyles'
import { Home } from './pages/Home'
import { NotFound } from './pages/NotFound'
import { Router } from '@reach/router'
import { Context } from './Context'

export const App = () => {
  const { isAuth } = useContext(Context)
  return (
    <div>
      <GlobalStyle />
      <Router>
        <NotFound default />
        <Home path='/' />
      </Router>
    </div>
  )
}
